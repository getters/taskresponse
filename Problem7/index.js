/** 
 * Anonymous self invoking function so that the variables and functions 
 * dont bleed into the global namespace
 */
(() => {
    const catImageUrl = 'https://i.guim.co.uk/img/media/26392d05302e02f7bf4eb143bb84c8097d09144b/446_167_3683_2210/master/3683.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=49ed3252c0b2ffb49cf8b508892e452d';
    const dogImageUrl = 'https://img.wallpapersafari.com/desktop/1366/768/38/59/uWqomU.jpg';

    const pastelRedColor = '#fea3aa';
    const pastelGreenColor = '#baed91';

    const catTimer = 3000; 
    const dogTimer = 5000; 
    const interval = 1000;
    const fadeInterval = 300;

    /* Prepend Image tag with cat image into DOM */
    $('body').prepend(`<img id="img-container" src="${catImageUrl}" alt="image"/>`);

    const setFadeTimer = (resolve, timeout) => {
        let countdownTimeout = timeout;

        /* Countdown timer setInterval */
        const intervalTimer = setInterval(() => {
            if (countdownTimeout) {
                $('#count-down').text(countdownTimeout / interval).show()
                    .fadeOut(fadeInterval);
                countdownTimeout = countdownTimeout - interval;
            } else {
                clearInterval(intervalTimer);
            }
        }, interval);

        /* Promise resolver timeout */
        return setTimeout(resolve, timeout);
    }

    /* Promise chain to perform syncronous tasks */
    new Promise(resolve => setFadeTimer(resolve, catTimer))
        .then(() => {
            $('body').css('background-color', pastelRedColor)
                .find('#img-container').attr('src', dogImageUrl);
        })
        .then(() => new Promise(resolve => setFadeTimer(resolve, dogTimer)))
        .then(() => $('body').css('background-color', pastelGreenColor))
})();