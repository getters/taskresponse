/** 
 * Anonymous self invoking function so that the variables and functions 
 * dont bleed into the global namespace
 */
(() => {
    /* Raw data passed to the application from API etc */
    const rawData = [
        ['2004', 1000, 400],
        ['2005', 1170, 460],
        ['2006', 660, 1120],
        ['2007', 1030, 540]
    ]

    /* Trace 1 mock Data */
    let trace1 = {
        x: [],
        y: [],
        text: [],
        name: 'Sales',
        type: 'bar',
        hoverinfo: 'none'
    };

    /* Trace 2 mock Data */
    let trace2 = {
        x: [],
        y: [],
        text: [],
        name: 'Expenses',
        type: 'bar',
        hoverinfo: 'none'
    };

    /* Formatting RawData to Plotly accepted format */
    rawData.forEach(item => {
        //Trace 1 related data
        trace1.x.push(item[0]);
        trace1.y.push(item[1]);
        trace1.text.push(item[1]);

        //Trace 1 related data
        trace2.x.push(item[0]);
        trace2.y.push(item[2]);
        trace2.text.push(item[2]);
    })

    /* Triggering Plotly render with the formatted data */
    Plotly.newPlot(
        'my-chart',
        [trace1, trace2],
        { barmode: 'group' },
        { displaylogo: false }
    );
})();